class digital_ocean_ddns {

	$token  = hiera('digital_ocean_ddns::token', '')
	$domain = hiera('digital_ocean_ddns::domain', '')
	$record = hiera('digital_ocean_ddns::record', '')

	class { 'python' :
		version    => '3.4',
	} ->

	file { '/etc/digital_ocean_ddns':
		ensure => directory,
	} ->

	file { '/etc/digital_ocean_ddns/update.rb':
		ensure   => present,
		content => template("digital_ocean_ddns/update.rb.erb"),
	} ->

	cron { 'Digital Ocean DDNS Cron':
		command => '/usr/bin/python3.4 /etc/digital_ocean_ddns/update.rb',
		user    => 'root',
		minute  => '*',
	}

}