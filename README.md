# Puppet Agent Module

```ruby
mod "puppetlabs/stdlib", "4.5.1"

# dep: puppetlabs/stdlib
mod "stankevich/python", "1.9.1"

# dep stankevich/python
mod "weyforth/digital_ocean_ddns",
	git: "https://weyforth@bitbucket.org/weyforth/puppet-digital_ocean_ddns.git",
	ref: "master"
```

